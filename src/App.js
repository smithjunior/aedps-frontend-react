import React from 'react'
import './App.css'
import Routes from './routes'
import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Grid from '@material-ui/core/Grid'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  }
}))
function App () {
  const classes = useStyles()

  return (
    <>
      <Grid container justify='center' className={classes.rootGrid} spacing={2}>
        <Grid item justify='center' xs={12}>
          <AppBar position='fixed' style={{ background: '#2196f3' }}>
            <Toolbar variant='dense'>
              <IconButton edge='start' className={classes.menuButton} color='inherit' aria-label='menu'>
                <MenuIcon />
              </IconButton>
              <Typography variant='h6' color='inherit'>
                Arbo ETL Data Processing System - AEDPS
              </Typography>
            </Toolbar>
          </AppBar>
        </Grid>
        <Grid item justify='center' xs={12}>
          <Routes />
        </Grid>
      </Grid>
    </>
  )
}

export default App
