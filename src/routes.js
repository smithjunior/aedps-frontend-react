
import React from 'react'
import {
  BrowserRouter,
  Switch,
  Route
} from 'react-router-dom'

import ProjectList from './pages/ProjectList'
import ProjectView from './pages/ProjectView'

export default function Routes () {
  return (
    <BrowserRouter>
      <Switch>
        <Route path='/' exact component={ProjectList} />
        <Route path='/project' exact component={ProjectList} />
        <Route path='/project/:id' exact component={ProjectView} />
      </Switch>
    </BrowserRouter>
  )
}
