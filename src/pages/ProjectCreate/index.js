import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Divider from '@material-ui/core/Divider'

export default function ProjectCreate () {
  const classes = useStyles()
  return (
    <>
      <Grid item justify='center'>
        <div className={classes.root}>
          <ExpansionPanel defaultExpanded>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls='panel1c-content'
              id='panel1c-header'
            />
            <ExpansionPanelDetails className={classes.details}>
              <TextField id='standard-basic' label='Nome do Projeto' fullWidth />
              <br />
              <input type='file' />
            </ExpansionPanelDetails>
            <Divider />
            <ExpansionPanelActions>
              <Button size='small' color='primary'>
                  Salvar
              </Button>
            </ExpansionPanelActions>
          </ExpansionPanel>
        </div>
      </Grid>
    </>
  )
}

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%'
  },
  rootGrid: {
    flexGrow: 1,
    width: '100%'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15)
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20
  },
  details: {
    alignItems: 'center'
  },
  column: {
    flexBasis: '33.33%'
  },
  helper: {
    borderLeft: `2px solid ${theme.palette.divider}`,
    padding: theme.spacing(1, 2)
  },
  link: {
    color: theme.palette.primary.main,
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline'
    }
  }
}))
