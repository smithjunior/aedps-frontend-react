import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import ProjectCreate from '../ProjectCreate'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

const useStyles = makeStyles(theme => ({
  table: {
    minWidth: 650
  }
}))

function createData (id, name, extension, filename) {
  return { id, name, extension, filename }
}

const rows = [
  createData(1, 'Validação Dengue 2018 Manaus', 'xlsx', 'Dengue-manaus-2018.xls'),
  createData(2, 'Validação Dengue 2018 7 Alagoas', 'xls', 'Dengue-sete-alagoas2018.xls')
]

export default function ProjectList () {
  const classes = useStyles()
  return (
    <>
      <br />
      <br />
      <ProjectCreate />
      <br />
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell>Id</TableCell>
              <TableCell>Nome do projeto</TableCell>
              <TableCell align='right'>Extensão</TableCell>
              <TableCell align='right'>Nome do Arquivo</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row => (
              <TableRow key={row.name}>
                <TableCell align='left'>{row.id}</TableCell>
                <TableCell component='th' scope='row'>
                  {row.name}
                </TableCell>
                <TableCell align='right'>{row.extension}</TableCell>
                <TableCell align='right'>{row.filename}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}
