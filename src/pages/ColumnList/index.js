import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

const useStyles = makeStyles(theme => ({
  table: {
    minWidth: 650
  }
}))

function createData (name, validation) {
  return { name, validation }
}

const rows = [
  createData('TP_NOT', 'NOTIFICATION_TYPE_VALIDATION'),
  createData('ID_AGRAVO', 'DIEASES_VALIDATION'),
  createData('SEM_NOT', 'EPI_WEEK_VALIDATION'),
  createData('ID_MN_RESI', 'NOTIFICATION_LIVE_CITY_ID_VALIDATION'),
  createData('ID_MUNICIP', 'NOTIFICATION_CITY_ID_VALIDATION'),
  createData('NM_BAIRRO', 'NAME_LOCATION_VALIDATION')
]

export default function ColumnCreate () {
  const classes = useStyles()
  return (
    <>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell>Coluna do Arquivo</TableCell>
              <TableCell align='center'>Regras de Validação</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row => (
              <TableRow key={row.name}>
                <TableCell component='th' scope='row'>
                  {row.name}
                </TableCell>
                <TableCell align='center'>{row.validation}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}
